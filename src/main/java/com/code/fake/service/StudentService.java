package com.code.fake.service;

import com.code.fake.entity.Student;
import com.code.fake.repository.StudentRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public Student createStudent(Student student) {
        return this.studentRepository.save(student);
    }

    public Student findById(Long id) {
        return this.studentRepository.findById(id).orElseThrow(() -> new IllegalStateException());
    }

    public Student findStudentByFirstName(String firstName) {
        return this.studentRepository.findStudentByFirstName(firstName);
    }

    public List<Student> findStudentsByFirstNameAndAge(String firstName, Integer age) {
        return this.studentRepository.findStudentsByFirstNameAndAge(firstName, age);
    }


    public Student findStudentByEmail(String email) {
        return this.studentRepository
                .findStudentByEmail(email)
                .orElseThrow(() -> new IllegalStateException());
    }

    public List<Student> findAll() {
        return this.studentRepository.findAll();
    }

    public List<Student> findByAgeGreater(Integer age) {
        return this.studentRepository.findByAgeGreater(age);
    }

    public Student updateStudent(Student student) {
        return this.studentRepository.save(student);
    }

    public int deleteStudentById(Long id) {
        return this.studentRepository.deleteStudentById(id);
    }

    public Long count() {
        return this.studentRepository.count();
    }

    public void createListOfStudents() {
        Faker faker = new Faker();
        for (int i = 0; i <= 30; i++) {
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            String email = firstName.concat(" ").concat(lastName).concat("@fake.com");
            Student student = new Student(firstName,lastName,email,faker.number().numberBetween(18,55));
            this.studentRepository.save(student);
        }
    }
}
