package com.code.fake.service;

import com.code.fake.entity.IdCard;
import com.code.fake.entity.Student;
import com.code.fake.repository.IdCardRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdCardService {
    @Autowired
    private IdCardRepository idCardRepository;
    public void createFakeIdCards(){
        Faker faker = new Faker();
        for (int i = 0; i <= 10; i++) {
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            String email = firstName.concat(" ").concat(lastName).concat("@fake.com");
            Student student = new Student(firstName,lastName,email,faker.number().numberBetween(18,55));
            IdCard idCard = new IdCard(faker.number().digits(10),student);
            this.idCardRepository.save(idCard);
        }
    }

    public IdCard findIdCardById(Long id) {
       return this.idCardRepository.findById(id).orElseThrow(() -> new IllegalStateException());
    }
}
