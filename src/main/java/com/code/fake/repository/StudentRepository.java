package com.code.fake.repository;

import com.code.fake.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("SELECT s FROM Student s WHERE s.email = :email ")
    Optional<Student> findStudentByEmail(@Param("email") String email);

    @Query("SELECT s FROM Student s WHERE s.firstName = :firstName  AND s.age = :age")
    List<Student> findStudentsByFirstNameAndAge(@Param("firstName") String firstName, @Param("age") Integer age);

    @Query("SELECT s FROM Student s WHERE s.firstName = :firstName ")
    Student findStudentByFirstName(@Param("firstName") String firstName);

    @Query(value = "SELECT * FROM student WHERE age >= :age", nativeQuery = true)
    List<Student> findByAgeGreater(@Param("age") Integer age);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM student WHERE id >= :id", nativeQuery = true)
    int deleteStudentById(@Param("id") Long id);
}
