package com.code.fake.controller;

import com.code.fake.entity.Student;
import com.code.fake.service.StudentService;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "api/v1/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    // get all
    @GetMapping(path = "all")
    public List<Student> getAllStudents() {
        return this.studentService.findAll();
    }
    // get by id
    @GetMapping(path = "{id}")
    public Student getStudent(@PathVariable Long id) {
        return this.studentService.findById(id);
    }
    // get by firstName
    @GetMapping(path = "findByEmail/{firstName}")
    public Student getStudentByFirstName(@PathVariable String firstName) {
        return this.studentService.findStudentByFirstName(firstName);
    }
    // get by email
    @GetMapping(path = "findByEmail/{email}")
    public Student getStudentByEmail(@PathVariable String email) {
        return this.studentService.findStudentByEmail(email);
    }
    // get by firstName , age
    @GetMapping(path = "findByFirstName/{firstName}/Age/{age}")
    public List<Student> getStudentsByFirstNameAndAge(@PathVariable String firstName, @PathVariable Integer age) {
        return this.studentService.findStudentsByFirstNameAndAge(firstName,age);
    }
    // age is greater
    @GetMapping(path = "findByAgeIsGreater/{age}")
    public List<Student> findByAgeGreater(@PathVariable Integer age) {
        return this.studentService.findByAgeGreater(age);
    }
    // count
    @GetMapping(path = "count")
    public Long studentsCount() {
        return this.studentService.count();
    }
    // create student
    @PostMapping(path = "create")
    public Student createStudent(@RequestBody Student student) {
        return this.studentService.createStudent(student);
    }
    // update student
    @PutMapping(path = "update")
    public Student UpdateStudent(@RequestBody Student student) {
        return this.studentService.updateStudent(student);
    }
    // delete student
    @DeleteMapping(path = "delete/{id}")
    public void deleteStudentById(@PathVariable Long id) {
        this.studentService.deleteStudentById(id);
    }

    @GetMapping(path = "list/create")
    public void createListOfStudents() {
        this.studentService.createListOfStudents();
    }
}
