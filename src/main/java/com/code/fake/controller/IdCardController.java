package com.code.fake.controller;

import com.code.fake.entity.IdCard;
import com.code.fake.service.IdCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/idCard")
public class IdCardController {
    @Autowired
    private IdCardService idCardService;

    @GetMapping(path = "fake")
    public void createFakeIdCards() {
        this.idCardService.createFakeIdCards();
    }

    @GetMapping(path = "{id}")
    public IdCard findIdCardById(@PathVariable("id") Long id) {
        return this.idCardService.findIdCardById(id);
    }
}
