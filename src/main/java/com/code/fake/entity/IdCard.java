package com.code.fake.entity;

import jakarta.persistence.*;

@Entity(name = "IdCard")
@Table(name = "id_card",
        uniqueConstraints = {
        @UniqueConstraint(name = "id_card_card_number_unique", columnNames = "card_number")
})
public class IdCard {
    @Id
    @SequenceGenerator(name = "id_card_sequence",sequenceName = "id_card_sequence",allocationSize = 1)
    @GeneratedValue(generator ="id_card_sequence",strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;
    @Column(name = "card_number",columnDefinition = "TEXT",nullable = false)
    private String cardNumber;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "student_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "id_card_student_id_fk")
    )
    private Student student;


    public IdCard() {
    }

    public IdCard(String cardNumber, Student student) {
        this.cardNumber = cardNumber;
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
